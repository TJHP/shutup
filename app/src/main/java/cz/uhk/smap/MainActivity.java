package cz.uhk.smap;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private TextView txvResult;
    private ArrayList<String> results;
    private String wordsFromDatabase;
    private BluetoothAdapter myBluetooth = BluetoothAdapter.getDefaultAdapter();
    private Set<BluetoothDevice> pairedDevices;
    private Button btnPaired, toStats, btnDisc;
    private ListView devicelist;
    private String deviceMac;
    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    private CreateConnectThread createConnectThread;
    private BluetoothSocket mmSocket;
    private ConnectedThread connectedThread;
    private StatisticMaker statsMaker;
    private StatisticsActivity statisticsActivity;
    private String message;
    private View view;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txvResult = (TextView) findViewById(R.id.txvResult);
        statsMaker = new StatisticMaker(getApplicationContext());
        statisticsActivity = new StatisticsActivity();


        //Statisctics button
        toStats = (Button) findViewById(R.id.toStats);
        toStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(MainActivity.this,StatisticsActivity.class);
                startActivity(intent);
            }
        });

        //Disconecting button
        btnDisc = (Button) findViewById(R.id.disc);
        btnDisc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();
            }
        });
        btnDisc.setVisibility(View.GONE);

        File filedatab = new File(getApplicationContext().getFilesDir(),"database.txt");
        if(!filedatab.exists()){
            makeDatabase();
        }

        wordsFromDatabase = getTextFileData("database.txt");

        chceckBTAdapter();
        makeConnection();

        File filestats = new File(getApplicationContext().getFilesDir(),"stats.txt");
        if(!filestats.exists()){
            statsMaker.createStatsFile();
        }
    }



    //This method receive speech input
    public void getSpeechInput(View view) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }

    //This method shows result of speech to text and making decision about offensive words from database
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                    results = new ArrayList<String>();

                    String[] splitted = result.get(0).toString().split("\\s+");
                    for (int i = 0; i < splitted.length; i++) {
                        results.add((String) Array.get(splitted, i));
                    }
                    compare();
                }
                break;
        }
    }

    //This method compares speech with database of offensive words
    private void compare() {
        String[] SplittedWordsFromDatabase = wordsFromDatabase.split("\\s+");

        ArrayList<String> database = new ArrayList<>();
        for (String s : SplittedWordsFromDatabase) {
            database.add(s);
        }

        ArrayList<String> spoken = new ArrayList<>();
        for (String s : results) {
            if (database.contains(s)) {
                spoken.add(s);

                txvResult.setText("Detekováno nevhodné slovo!");
                statsMaker.insertInput(s);
                statisticsActivity.saveSpoken(s, getApplicationContext());
                statisticsActivity.wordsCount(s, getApplicationContext());
                if(createConnectThread != null){
                    createConnectThread.sendMessage();
                }
            } else {
                txvResult.setText("Dobrý den");
            }
        }
    }

    //This method will get offensive words from .txt file (database)
    private String getTextFileData(String fileName) {
        StringBuilder text = new StringBuilder();
        try {
            FileInputStream fIS = getApplicationContext().openFileInput(fileName);
            InputStreamReader isr = new InputStreamReader(fIS, "UTF-8");
            BufferedReader br = new BufferedReader(isr);

            String line;
            while ((line = br.readLine()) != null) {
                text.append(line + '\n');
            }
            br.close();
        } catch (IOException e) {
            Log.e("Error!", "Error occured while reading text file from Internal Storage!");
        }
        return text.toString();
    }

    //This method create .txt file with offensive words (database)
    private void makeDatabase() {
        File path = getApplicationContext().getFilesDir();
        File file = new File(path, "database.txt");

        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            stream.write(database().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //This method contains offensive words that go into database
    private String database() {
        String words = "Miloš Zeman debil retard pitomec prdel k**** p*** c**** vole " ;
        return words;
    }

    //This method create and testing the Bluetooth Adapter
    private void chceckBTAdapter() {
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if (myBluetooth == null) {
            Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();
            finish();
        } else {
            if (!myBluetooth.isEnabled()) {
                Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnBTon, 1);
            }
        }
    }

    //Make bluetooth connection with Arduino
    private void makeConnection() {
        btnPaired = (Button) findViewById(R.id.button);
        devicelist = (ListView) findViewById(R.id.listView);

        btnPaired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pairedDevicesList();
            }
        });
    }

    //Fetch list of devices to connect
    private void pairedDevicesList() {
        pairedDevices = myBluetooth.getBondedDevices();
        ArrayList list = new ArrayList();

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice bt : pairedDevices) {
                list.add(bt.getName() + "\n" + bt.getAddress());
            }
        } else {
            Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found.", Toast.LENGTH_LONG).show();
        }

        AdapterView.OnItemClickListener myListClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView av, View v, int arg2, long arg3) {
                String info = ((TextView) v).getText().toString();
                String address = info.substring(info.length() - 17);
                deviceMac = address;

                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                createConnectThread = new CreateConnectThread(bluetoothAdapter, deviceMac ,mmSocket, connectedThread);
                createConnectThread.start();
                listen();
                btnDisc.setVisibility(View.VISIBLE);
                btnPaired.setVisibility(View.GONE);
                devicelist.setVisibility(View.GONE);
            }
        };

        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        devicelist.setAdapter(adapter);
        devicelist.setOnItemClickListener(myListClickListener);
    }

    // Terminate Bluetooth Connection and close app
    @Override
    public void onBackPressed() {
        if (createConnectThread != null){
            createConnectThread.cancel();
        }
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    //Disconnect from Arduino and close sockets
    private void disconnect(){
        createConnectThread.cancel();
        btnPaired.setVisibility(View.VISIBLE);
        btnDisc.setVisibility(View.GONE);
        devicelist.setVisibility(View.VISIBLE);
    }

    //Monitor speech from Arduino
    private void listen(){
        Thread thread = new Thread(){
            public void run(){
                while(true){
                    message = createConnectThread.getNoiseStatus();
                        if (message != null && message.equals("3\r")) {
                            getSpeechInput(view);
                            createConnectThread.connectedThread.message = null;
                            message = null;
                        }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

}