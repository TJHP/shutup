package cz.uhk.smap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StatisticMaker {

    private Context context;
   // private MainActivity main;

    //Constructor
    public StatisticMaker(Context context){
        this.context = context;
    }

    //Create new .txt file to save spoken vulgarisms
    public void createStatsFile(){
        File path = context.getFilesDir();
        File file = new File(path, "stats.txt");

        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("Error!", "Cannot open file stats.txt");
        }
        try {
            stream.write("".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Error!", "Cannot write default empty string into stats.txt");
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Error!", "Cannot close output stream at creating stats.txt");
            }
        }

    }

    //Load .txt file with spoken vulgarisms
    public ArrayList<String> getStats(){
        StringBuilder text = new StringBuilder();
        ArrayList<String> array = new ArrayList<>();
        try {
            FileInputStream fIS = new FileInputStream(new File(context.getFilesDir(), "stats.txt"));
            InputStreamReader isr = new InputStreamReader(fIS, "UTF-8");
            BufferedReader br = new BufferedReader(isr);

            String line;
            while ((line = br.readLine()) != null) {
                text.append(line + '\n');
                array.add(line);
            }
            br.close();
        } catch (IOException e) {
            Log.e("Error!", "Error occured while reading text file from Internal Storage!");
        }

        //return text.toString();
        return array;
    }

    //Insert new spoken vulgarism into .txt file with date stamp
    public void insertInput(String string){
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = simpleDateFormat.format(new Date());

        File file = new File(context.getFilesDir(), "stats.txt");
        try (
            FileWriter f = new FileWriter(file, true);
            BufferedWriter b = new BufferedWriter(f);
            PrintWriter p = new PrintWriter(b);) {
            p.println(date + " : " + string);
        }catch (IOException i){
            i.printStackTrace();
            Log.e("Error!", "Cannot insert new spoken offensive word into stats.txt");
        }
    }

    //Delete all spoken vulgarisms from .txt file
    public void deleteRecords() {
        File file = new File(context.getFilesDir(), "stats.txt");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("Error!", "Cannot delete records od spoken vulgarisms");
        }
        writer.print("");
        writer.close();
    }
}
