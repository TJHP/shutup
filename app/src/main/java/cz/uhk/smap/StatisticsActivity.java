package cz.uhk.smap;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class StatisticsActivity extends AppCompatActivity {

    private TextView totalCount, dayCount, knownCount, weekCount;
    private StatisticMaker statisticMaker;
    private Button clear;
    private TinyDB tiny;
    private ArrayList<String> spokenWords;
    private ArrayList<Integer> dayGraphData;
    private ArrayList<Integer> monthGraphData;
    private ListView listView;

    private int countAll;
    private int countKnown;
    private int countDay;
    private int countWeek;

    @Override
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        statisticMaker = new StatisticMaker(getApplicationContext());
        tiny = new TinyDB(getApplicationContext());

        listView = (ListView) findViewById(R.id.listView);

        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, getRankings());
        listView.setAdapter(adapter);

        clear = (Button) findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statisticMaker.deleteRecords();
                tiny.clear();
                finish();
                startActivity(getIntent());
            }
        });

        computeStatistics();

        totalCount = (TextView) findViewById(R.id.totalCount);
        totalCount.setText(String.valueOf(countAll));

        dayCount = (TextView) findViewById(R.id.todayCount);
        dayCount.setText(String.valueOf(countDay));

        knownCount = (TextView) findViewById(R.id.totalKnown);
        knownCount.setText(String.valueOf(countKnown));

        weekCount = (TextView) findViewById(R.id.weekCount);
        weekCount.setText(String.valueOf(countWeek));


        DataPoint[] points = new DataPoint[dayGraphData.size()];
        for(int i = 0; i< dayGraphData.size(); i++){
            points[i] = new DataPoint(i, dayGraphData.get(i));
        }

        GraphView graph = findViewById(R.id.graph);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(points);
        graph.addSeries(series);


        DataPoint[] points2 = new DataPoint[monthGraphData.size()];
        for(int i = 0; i< monthGraphData.size(); i++){
            points2[i] = new DataPoint(i, monthGraphData.get(i));
        }

        GraphView graph2 = findViewById(R.id.graph2);
        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(points2);
        graph2.addSeries(series2);

    }

    //Save used offensive words to TinyDB for making statisctics
    public void saveSpoken(String word, Context context){
        if(tiny == null)
            tiny = new TinyDB(context);
        if(!tiny.checkIfExist("spokenDB")){
            spokenWords = new ArrayList<>();
            tiny.putListString("spokenDB", spokenWords);
            Log.i("X", "New spoken list crated!");
        }

       spokenWords =  tiny.getListString("spokenDB");
       if(!spokenWords.contains(word)){
           spokenWords.add(word);
           tiny.putListString("spokenDB", spokenWords);
       }
    }

    //Computing individual statistics
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void computeStatistics(){
        File path = getApplicationContext().getFilesDir();
        File file = new File(path, "stats.txt");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = simpleDateFormat.format(new Date());
        Calendar cal = Calendar.getInstance();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(new Date());
        int actWeek = cal1.get(Calendar.WEEK_OF_YEAR);
        Set<String> set = new HashSet();
        Set<String> set2 = new HashSet();
        ArrayList<String> oneItemInHashDay = new ArrayList();
        ArrayList<String> allDayItem = new ArrayList<>();
        ArrayList<String> oneItemInHashMonth = new ArrayList();
        ArrayList<String> allMonthItem = new ArrayList<>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("Err", "Cannot open .txt file with spoken vulgarisms");
        }

        int lines = 0;
        int todayLines = 0;
        int weekRows = 0;
        while (true) {
            try {
                String actLine = reader.readLine();
                if (actLine == null){
                    break;
                }
                else {
                    if (actLine.contains(date)) todayLines++;
                    String[] lineSplits = actLine.split(" ");
                    String flow = lineSplits[0];
                    try{
                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date flowDate = df.parse(flow);
                        cal.setTime(flowDate);
                        int week = cal.get(Calendar.WEEK_OF_YEAR);
                        if(week == actWeek) weekRows++;

                        String cutttedLine = actLine.substring(actLine.indexOf("-") + 4, actLine.indexOf(" "));
                        if(!set.add(cutttedLine)) oneItemInHashDay.add(cutttedLine);
                        allDayItem.add(cutttedLine);

                        //String cutttedLine2 = actLine.substring(actLine.indexOf("-") + 1, actLine.indexOf("-"));
                        String cutttedLine2 = actLine.split("-")[1];
                        if(!set2.add(cutttedLine2)) oneItemInHashMonth.add(cutttedLine2);
                        allMonthItem.add(cutttedLine2);
                    } catch (ParseException e) {
                        e.printStackTrace();
                        Log.e("Err", "Error in summary vulgarisms in last week");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Err", "Cannot iterate through total spoken vulgarisms");
            }
            lines++;
        }

        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Err", "Cannot close reader from counting total spoken vulgarisms");
        }

        countAll = lines;
        countDay = todayLines;
        countWeek = weekRows;

        tiny = new TinyDB(getApplicationContext());
        ArrayList<String> spokenList = new ArrayList<>();
        if(tiny.checkIfExist("spokenDB")){
            spokenList = tiny.getListString("spokenDB");
            countKnown = spokenList.size();
        }
        else{
            countKnown = 0;
        }

        dayGraphData = computeGraphData(oneItemInHashDay, allDayItem);
        monthGraphData = computeGraphData(oneItemInHashMonth, allMonthItem);

    }

    //Counts how many was any offensive word was used
    public void wordsCount(String word, Context context){
        if(tiny == null)
            tiny = new TinyDB(context);
        ArrayList cnts;
        ArrayList<String> words;
        if(!tiny.checkIfExist("counts")){
            cnts = new ArrayList();
            words = tiny.getListString("spokenDB");

            for(int i = 0; i < words.size(); i++){
                cnts.add(0);
            }

            tiny.putListInt("counts", cnts);

        }
        else{
            words = tiny.getListString("spokenDB");
            cnts = tiny.getListInt("counts");

            if(words.size() > cnts.size()){
                cnts.add(0);
            }

            int index = words.indexOf(word);
            int count = (int)cnts.get(index);
            count++;
            cnts.set(index, count);

            tiny.putListInt("counts",cnts);
        }
    }

    //Makes list of used offensive words sorted by number of uses
    private ArrayList<String> getRankings(){
        if(tiny.checkIfExist("counts") && tiny.checkIfExist("spokenDB")){
            ArrayList<String> rankings = new ArrayList<>();
            ArrayList counts = tiny.getListInt("counts");
            ArrayList words = tiny.getListString("spokenDB");

            for(int i = 0; i < words.size(); i++){
                rankings.add(words.get(i).toString() + "  : " + counts.get(i).toString());
            }

            Collections.sort(rankings, new Comparator<String>() {
                public int compare(String o1, String o2) {
                    return extractInt(o2) - extractInt(o1);
                }

                int extractInt(String s) {
                    String num = s.replaceAll("\\D", "");
                    return num.isEmpty() ? 0 : Integer.parseInt(num);
                }
            });

            return rankings;
        }
        else {
            Log.e("Error", "Cant find rankings list!");
            return new ArrayList<>();
        }
    }

    //Compute data on y-line in day graph
    private ArrayList computeGraphData(ArrayList<String> listOnlyOnes, ArrayList<String> listAll){
        int counter = 0;
        ArrayList counts = new ArrayList<>();
        for(int i = 0; i < listOnlyOnes.size(); i++){
            counts.add(Collections.frequency(listAll, listOnlyOnes.get(i)));
        }
        return counts;
    }

}
